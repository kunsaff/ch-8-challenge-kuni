import React from 'react'
import {useState} from 'react'
import { useEffect } from 'react'

const PlayerForm = ({handleCreate, playerEdit, cancelEdit, performEdit}) => {
  const [Username, setUsername] = useState('')
  const [Email, setEmail] = useState('')
  const [Password, setPassword] = useState('')
  const [Exp, setExp] = useState('0')
  const [Lvl, setLvl] = useState('0')
  
  useEffect(() => {
    if(typeof playerEdit === 'object') {
      setUsername(playerEdit.username)
      setEmail(playerEdit.email)
      setPassword(playerEdit.password)
      setExp(playerEdit.exp)
      setLvl(playerEdit.lvl)
    }
  }, [playerEdit])
  

  const submitHandler = (e) => {
    e.preventDefault()
    let player = {
      username: Username, 
      email: Email, 
      password: Password, 
      exp: Exp, 
      lvl: Lvl
    }
    if(typeof playerEdit === 'object' && playerEdit.id)
    performEdit({...player, id: playerEdit.id})
    else
      handleCreate(player)
    handleCancelEdit()
  }

  const handleCancelEdit = () => {
    cancelEdit()
    setUsername('')
    setEmail('')
    setPassword('')
    setExp('')
    setLvl('')
  }

  return (
    <form onSubmit={e => submitHandler(e)}>
    <div className="mb-3">
        <label htmlFor="username" className="form-label">Username</label>
        <input onChange={(e)=> setUsername(e.target.value)} type="text" className="form-control" id="username" value={Username}/>
      </div>
      <div className="mb-3">
        <label htmlFor="email" className="form-label">Email</label>
        <input onChange={(e)=> setEmail(e.target.value)} type="email" className="form-control" id="email" value={Email} />
      </div>
      <div className="mb-3">
        <label htmlFor="password" className="form-label">Password</label>
        <input onChange={(e)=> setPassword(e.target.value)} type="password" className="form-control" id="password" value={Password}/>
      </div>
      <div className="mb-3">
        <label htmlFor="exp" className="form-label">Experience</label>
        <input onChange={(e)=> setExp(e.target.value)} type="number" className="form-control" id="exp" value={Exp}/>
      </div>
      <div className="mb-3">
        <label htmlFor="lvl" className="form-label">Level</label>
        <input onChange={(e)=> setLvl(e.target.value)} type="number" className="form-control" id="lvl" value={Lvl}/>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
      <button onClick={handleCancelEdit} type="button" className="btn btn-danger">Reset</button>
    </form>
  )
}

export default PlayerForm