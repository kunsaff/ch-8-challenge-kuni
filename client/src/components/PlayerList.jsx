import React from 'react'

const Playerlist = ({players, selectEdit}) => {
  
  return (
    <table className="table table-bordered">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Username</th>
        <th scope="col">Email</th>
        <th scope="col">Experience</th>
        <th scope="col">Level</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      {players.map((player, index) =>(
        <tr key={index}>
          <td>{player.id}</td>
          <td>{player.username}</td>
          <td>{player.email}</td>
          <td>{player.exp}</td>
          <td>{player.lvl}</td>
          <td><button onClick={() => selectEdit(player.id)} type="button" className="btn btn-primary">Edit</button></td>
        </tr>
      ))}
    </tbody>
  </table>
  )
}

export default Playerlist