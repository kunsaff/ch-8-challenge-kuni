import 'bootstrap/dist/css/bootstrap.min.css';
import PlayerList from './components/PlayerList.jsx';
import PlayerForm from './components/PlayerForm.jsx';
import {useState} from 'react'

function App() {
  let defaultPlayerData =[
    {
      id: 1,
      username: 'admin',
      email: 'admin@mail.com',
      password: '12345',
      exp: 100,
      lvl: 9
    },
    {
      id: 2,
      username: 'player1',
      email: 'player1@there.com',
      password: '12345',
      exp: 123,
      lvl: 9
    },
    {
      id: 3,
      username: 'player2',
      email: 'player2@mail.com',
      password: '12345',
      exp: 234,
      lvl: 7
    },
    {
      id: 4,
      username: 'superadmin',
      email: 'superadmin@there.com',
      password: '12345',
      exp: 456,
      lvl: 8
    }
  ]

  const [Players, setPlayers] = useState(defaultPlayerData)
  const [PlayerEdit, setPlayerEdit] = useState('')
  const [Keyword, setKeyword] = useState('')


  const addPlayer = (newPlayer) => {
    newPlayer.id = Math.max(...Players.map(player => player.id))+1
    setPlayers (Player => [...Players, newPlayer])
  }

  const selectEdit = (playerId) => {
    let player = Players.filter(player => player.id === playerId)
    setPlayerEdit(player[0])
  }

  const performEdit = (player) => {
    console.log(`otw edit ${player.username}`)
    const indexToEdit= Players.findIndex(item => item.id === player.id)
    // console.log(`username ${indexToEdit} akan diedit`)
    let copyPlayers= [...Players]
    copyPlayers[indexToEdit] = player
    
    setPlayers(copyPlayers)

  }

  const cancelEdit = () => {
    selectEdit('')
  }
  const filteredPlayerData =() => {
    return Players.filter(player => new RegExp (Keyword, 'g').test(player.username)
    || new RegExp (Keyword, 'g').test(player.email)
    || new RegExp (Keyword, 'g').test(player.exp)
    || new RegExp (Keyword, 'g').test(player.lvl))
  }


  return (
    <div>
      <div className='container mt-5'>
        <div className='row'>
          <div className='col-8'>
            <div className="d-flex justify-content between">
              <p className='h2 me-5'>Player List</p>
              <form className="row g-2">
                <div className="col-auto">
                  <input type="text" onChange={(e) => setKeyword(e.target.value)} value={Keyword}
                  className="form-control" id="find" placeholder="type here to find player..."/>
                </div>
              </form>
            </div>
            <PlayerList players={filteredPlayerData()} selectEdit={selectEdit} />
          </div>
          <div className='col-4'>
            <p className='h2'>Player</p>
            <PlayerForm handleCreate={addPlayer} playerEdit={PlayerEdit} cancelEdit={cancelEdit} performEdit={performEdit} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
